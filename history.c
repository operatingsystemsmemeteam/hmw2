#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_SIZE 40


//-------------------------------------//Basic Structure of Queue//---------------------------------------//
struct hNode{

    int commNum;
    char *input [MAX_SIZE];
    struct hnode *next;

};


struct queue{

    int count;
    struct hNode *front;
    struct hNode *end;

};


//Create new History Node
struct hNode *newNode(char *x[MAX_SIZE], struct queue *queue){

    struct hNode *temp = (struct hNode*) malloc (sizeof(struct hNode));

    //Copy string into node
    strcpy(temp->input, x);
    temp->commNum = queue->count;

    temp->next = NULL;
    return temp;
};

//Create new queue
struct queue *createQueue(){

    struct queue *queue = (struct queue*)malloc(sizeof(struct queue));
    queue->count = 0;
    queue->front = NULL;
    queue->end = NULL;
    return queue;
};
//---------------------------------------------------------------------------------------------------------//


//-------------------------------------------//Queue Management//------------------------------------------//
// Add node
void enQueue(struct queue *queue, char *x){

    struct hNode *temp = newNode(x, queue);

    //If queue is empty just assign pointers to it.
    if(queue->end == NULL){

        queue->front = temp;
        queue->end = temp;
    }
    else{
        //place node at the end of the queue.
        queue->end->next = temp;
        queue->end = temp;

    };
    queue->count++;
}

//remove node
struct hNode *deQueue(struct queue *queue){

    //If queue is empty
    if(queue->front == NULL){
        printf("Empty\n");
        return NULL;
    };

    struct hNode *temp = queue->front;
    int x = queue->front->input;

    queue->front = queue->front->next;

    free(temp);

    return x;

};
//--------------------------------------------------------------------------------------------------------------//



int main(){


    struct queue *queue = createQueue();

    char word1[MAX_SIZE] = "hello";
    enQueue(queue, word1);

    char word2[MAX_SIZE] = "goodbye";
    enQueue(queue, word2);

    char word3[MAX_SIZE] = "why";
    enQueue(queue, word3);


//------------------------------//testing//---------------------------------------------------------------------//
    struct hNode *n = queue->front;
    while(n != NULL){
        printf("Dequeued item is: %s command: %d \n", n->input, n->commNum);
        n = n->next;
    }
    free(n);

    while(deQueue(queue) != NULL){
        deQueue(queue);
    }

    struct hNode *v = queue->front;
    while(v != NULL){
        printf("Dequeued item is: %s command: %d \n", v->input, v->commNum);
        v = v->next;
    }
    free(v);

    return 0;
//--------------------------------------------------------------------------------------------------------------//

};