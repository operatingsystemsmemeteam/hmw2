#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX_SIZE 40
//-------------------------------------//Basic Structure of Queue//---------------------------------------//
struct hNode{

    int commNum;
    char input[MAX_SIZE];
    struct hNode* next;
};
struct queue{

    int count;
    struct hNode *front;
    struct hNode *end;
};
//Create new History Node
struct hNode *newNode(char x[MAX_SIZE], struct queue *queue){

    struct hNode *temp = (struct hNode*) malloc (sizeof(struct hNode));

    //Copy string into node
    strcpy(temp->input, x);
    temp->commNum = queue->count;

    temp->next = NULL;

    return temp;
};
//Create new queue
struct queue *createQueue(){

    struct queue *queue = (struct queue*)malloc(sizeof(struct queue));
    queue->count = 0;
    queue->front = NULL;
    queue->end = NULL;
    return queue;
};
//---------------------------------------------------------------------------------------------------------//
//-------------------------------------------//Queue Management//------------------------------------------//
// Add node
void enQueue(struct queue *queue, char *x){

    struct hNode *temp = newNode(x, queue);

    //If queue is empty just assign pointers to it.
    if(queue->end == NULL){

        queue->front = temp;
        queue->end = temp;
    }
    else{
        //place node at the end of the queue.
        queue->end->next = temp;
        queue->end = temp;

    };
    queue->count++;
};
//remove node
struct hNode *deQueue(struct queue *queue){

    //If queue is empty
    if(queue->front == NULL){
        printf("Empty\n");
        return NULL;
    };

    struct hNode *temp = queue->front;
    int x = *queue->front->input;

    queue->front = queue->front->next;

    free(temp);
};
//--------------------------------------------------------------------------------------------------------------//


//-------------------------------------------------//Project Functions//----------------------------------------//
void GetHistory(struct queue *queue){

    if(queue->front == NULL){
        printf("Queue is Empty \n");
    }
    else{
        int tail = 0;
        int hGrab = 10;

        if(queue->count > 9){
            tail = queue->count - hGrab;
        }
        else{
            printf("Queue is less than 10 will print what is available \n");
            printf("\n");
        };

        char strArray[hGrab + 1][MAX_SIZE +1];

        printf("Tail Size: %d \n", tail);
        printf("\n");

        struct hNode *temp = queue->front;

        int i = 0;
        //Pushing element into an array to print last elements first.
        while(temp != NULL){

            if(temp->commNum >= tail){

                strcpy(strArray[i], temp->input);
                printf("strcpy: %s \n",strArray[i]);
                temp = temp->next;
                i++;
            }
            else{
                temp = temp->next;
            }
        }
        free(temp);
        printf("\n");

	int z = 0;

        while(z < queue->count){

            printf("Array Position: %d, \t  Element: %s \n",z ,strArray[z]);
	    z++;
        }
        printf("\n");

        printf("size of i: %d \n", i);

        printf("\n");

        i = i-1;
        //Printing backwards
        while( i >= 0){
            printf("Element: %s \t Position: %d \n", strArray[i], i);
            i--;
        };
    };
};
//Return Last inputed command
char *lastInput(struct queue *queue){

    if(queue->front == NULL){
        printf("Queue is empty");
    }
    else{
        return queue->end->input;
    };
    return 0;
};
//Return nth input
char *nInput(struct queue *queue, int x){

    if(queue->front == NULL){
        printf("Queue is empty");
    }
    else{
        struct hNode *temp = queue->front;

        while(temp != NULL){
            if(temp->commNum == x){
                return temp->input;
            }
            else{
                temp = temp->next;
            };
        };
    };
    return 0;
};
//--------------------------------------------------------------------------------------------------------------//
int main(){


    struct queue *queue = createQueue();

    printf("Last Command: %s \n", lastInput(queue));
    printf("\n");

    char word1[MAX_SIZE] = "word_0";
    enQueue(queue, word1);

    char word2[MAX_SIZE] = "word_1";
    enQueue(queue, word2);

    char word3[MAX_SIZE] = "word_2";
    enQueue(queue, word3);

    char word4[MAX_SIZE] = "word_3";
    enQueue(queue, word4);

    char word5[MAX_SIZE] = "word_4";
    enQueue(queue, word5);

    char word6[MAX_SIZE] = "word_5";
    enQueue(queue, word6);

    char word7[MAX_SIZE] = "word_6";
    enQueue(queue, word7);
/*
    char word8[MAX_SIZE] = "word_7";
    enQueue(queue, word8);

    char word9[MAX_SIZE] = "word_8";
    enQueue(queue, word9);

    char word10[MAX_SIZE] = "word_9";
    enQueue(queue, word10);

    char word11[MAX_SIZE] = "word_10";
    enQueue(queue, word11);

    char word12[MAX_SIZE] = "word_11";
    enQueue(queue, word12);

    char word13[MAX_SIZE] = "word_12";
    enQueue(queue, word13);
*/

//------------------------------//testing//---------------------------------------------------------------------//
    struct hNode *n = queue->front;
    while(n != NULL){
        printf("In the Queue: %s \t command: %d \n", n->input, n->commNum);
        n = n->next;
    }
    free(n);
    printf("\n");

    GetHistory(queue);
    printf("\n");

    printf("Last Command: %s \n", lastInput(queue));
    printf("\n");

/*
    while(deQueue(queue) != NULL){
        deQueue(queue);
    }

    struct hNode *v = queue->front;
    while(v != NULL){
        printf("Dequeued item is: %s command: %d \n", v->input, v->commNum);
        v = v->next;
    }
    free(v);

    return 0;
*/

//--------------------------------------------------------------------------------------------------------------//

};
