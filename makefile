
CXX = gcc
STD = c11

osh : simple-shell.c
	$(CXX) -std=$(STD) $^ -o $@

piped: simple-shell-piped.c
	$(CXX) -std=$(STD) $^ -o $@

clean:
	rm osh
	rm piped
