# HMW2

This project is based on Project 1 of Chapter 3 of your book (p157). The aim of the project is to create a simple shell interface using C programming. The shell must accept user commands and then execute each command using a separate process. Use the VM that you build in HMW 1 to complete this project.