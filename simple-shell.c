/**
 * Simple shell interface program.
 *
 * Operating System Concepts - Ninth Edition
 * Copyright John Wiley & Sons - 2013
 */

//this flag allows use of std lib function getline()
#define __STDC_WANT_LIB_EXT2__ 1

#include <stdio.h>
#include <unistd.h>

#include <stdlib.h>
#include <string.h>

#include <signal.h>
#include <sys/wait.h>
#include <sys/types.h>

#include "history.h"

#define MAX_LINE 80 /* 80 chars per line, per command */

void sanitize(char* input)
{
	// remove '\n' character from end of input
	
	int c = strlen(input) - 1;

	if (input[c] == '\n')
		input[c] = ' ';
}

int main(void)
{
	// one command at arg[0] + 40 arguments
	char *args[MAX_LINE/2 + 1];	/* command line (of 80) has max of 40 arguments */
	int should_run = 1;
	int should_wait = 0;

	int i, upper;

	char *user_input = (char*) malloc(MAX_LINE * sizeof(char));
	struct queue* history = (struct queue*) malloc(sizeof(struct queue));

	pid_t pid;
	size_t bufsz = MAX_LINE;
		
	int ppid = getpid();

	while (should_run){
		
		printf("pid %u: osh> ", getpid());
		fflush(stdout); 

		getline(&user_input, &bufsz, stdin);

		if (user_input[0] != '\n')
			enQueue(history, user_input);

		sanitize(user_input);
		
	//	printf("%s\n", user_input);

		// tokenize the input for consumption by child process

		char *tok = strtok(user_input, " ");

		// first char is not NULL
		// Basically allows you to press RETURN after child process is done and parent is still
		//  waiting for input. Just go back to shell prompt.
		if (tok !=  NULL)
		{
			i = 0;
			while (tok != NULL)
			{
				if (strcmp(tok,"&") == 0)
				{
					should_wait = 1;
					//printf ("should wait = %u\n", should_wait);
					tok = strtok(NULL, " ");
				}
				else
				{
					args[i] = tok;
					//printf ("%d %s\n", i, args[i]);
					i++;
					tok = strtok(NULL, " ");
				}
			}

			free(tok);
			
			//printf("args: %s\n", &args[1]);

			/**
			 * After reading user input, the steps anre:
			 * (1) fork a child process
			 * (2) the child process will invoke execvp()
			 * (3) if command included &, parent will invoke wait()
			 */	

			if (strcmp(args[0], "exit") == 0)
			{
				should_run = 0;
				//exit(0);
			}
			else if (strcmp(args[0], "history") == 0)
			{
				GetHistory(history);
			}
			else if (strcmp(args[0], "!!") == 0)
			{
				// exec last command
			}
			else
			{			
				pid = fork();
				
				if (pid < 0)
				{
					fprintf(stderr, "Could not fork!\n");
					should_run = 0;
				}
				else 
				{
					if (pid == 0)
					{
						//printf("\npid %u: in child process.\n", getpid());

						printf("\n");

						execvp(args[0], args);
					}
					else // if (getpid() == ppid)
					{
						//printf("pid %u: in parent process.\n", getpid());

						if (should_wait)
						{
							//printf("pid %u: waiting for child process to complete.\n", getpid());
							wait(NULL);
						}
						else
						{
							// we are not waiting for the child process to complete, so we will
							// ignore SIGCHLD so kernel will automatically reap zombies.
							signal(SIGCHLD, SIG_IGN);
						}
						should_wait = 0;

						//printf ("pid %u: freed user_input\n", getpid());
						
						//free(*args);
						//printf ("pid %u: freed args\n", getpid());

						for (int a = 0; args[a]; a++)
						{
							
							args[a] = NULL;
						//	printf("pid %u: initialized args[%u].\n", getpid(), a);
						}
						
						//should_run = 0;
					}
				}
			}
		}
	}

	free(user_input);
    
	return 0;
}
