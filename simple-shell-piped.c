/**
 * Simple shell interface program.
 *
 * Operating System Concepts - Ninth Edition
 * Copyright John Wiley & Sons - 2013
 */

//this flag allows use of std lib function getline()
#define __STDC_WANT_LIB_EXT2__ 1

#include <stdio.h>
#include <unistd.h>

#include <stdlib.h>
#include <string.h>

#include <signal.h>
#include <sys/wait.h>
#include <sys/types.h>

#include "history.h"

#define MAX_LINE 80 /* 80 chars per line, per command */

// sanitize
//
// sanitize user input by removing unecessary or undesirable characters
void sanitize(char* input)
{
	// remove unwanted characters from input

	for (int i = 0; i < strlen(input); i++)
	{
		if (input[i] == '\n')
			input[i] = '\0';
	}
}

// tokenize
//
// split the contents of input into tokens and store them in buffer the specified buffer.
// return value is a flag set on certain conditions:
//
//-1: first char is NULL, do nothing 
// 0: Continue normally without waiting
// 1: Exit
// 2: & character found, wait for child to terminate
// 3: history
// 4: !! found, exec last command in history queue
// 5: !n found, exec nth command in history queue

int tokenize (char *input, char* buf[])
{
	//printf ("%u: tokenizing string: %s\n", getpid(), input);

	char* temp = (char*) malloc(strlen(input));
	strcpy(temp, input);

	sanitize(temp);

	char *tok = strtok(temp, " ");

	int flag = 0;

	if (tok == NULL)
	{
		flag = -1;
	}
	else if (strcmp(tok, "exit") == 0)
	{
		flag = 1;
	}
	else if (strcmp(tok, "history") == 0)
	{
		flag = 3;
	}
	else if (tok[0] == '!')
	{
		if (strcmp(tok, "!!") == 0)
			flag = 4;
		else
			flag = 5;
	}
	else
	{
		int i = 0;
		while (tok)
		{
			if (strcmp(tok,"&") == 0)
			{
				flag = 2;
			}
			else
			{
				strcpy(buf[i], tok);
				//printf ("%d %s\n", i, buf[i]);
				i++;
			}

			tok = strtok(NULL, " ");
		}

		buf[i] = NULL;
		free(tok);
	}

	//printf("%s: flag = %d\n", __func__, flag);

	return flag;
}

int main(void)
{
	// one command at arg[0] + 40 arguments
	char *args[MAX_LINE/2 + 1];	/* command line (of 80) has max of 40 arguments */
	int should_run = 1;
	int should_wait = 0;
	int run_com_from_hist = 0;

	char *user_input = (char*) malloc(MAX_LINE * sizeof(char));
	char *hist_command;
	
	struct queue* history = createQueue();

	pid_t pid;
	size_t bufsz = MAX_LINE;
	
	int pipe_ptc[2]; // file descriptor for pipe from parent process to child process
	int pipe_ctp[2]; // file descriptor for pipe from child process to parent process

	int pipe_hist[2]; // file descriptor for parent-to-child history pipe

	int ppid = getpid();

	while (should_run){
		
		switch (run_com_from_hist)
		{
			case 0:
				printf("pid %u: osh> ", getpid());
				fflush(stdout); 

				getline(&user_input, &bufsz, stdin);
				break;
			case 1:
				strcpy(user_input, hist_command);
				printf("running command from history: %s\n", user_input);
				free(hist_command);
				run_com_from_hist = 0;
				break;
			default:
				break;
		}

		// open state pipe from parent-to-child
		if (pipe(pipe_ptc) == -1)
		{
			fprintf(stderr, "could not create pipe_ptc!\n");
			return 1;
		}
		else
		{
			//printf("created pipe_ptc.\n");
		}

		// open state pipe from child-to-parent
		if (pipe(pipe_ctp) == -1)
		{
			fprintf(stderr, "could not create pipe_ctp!\n");
			return 1;
		}
		else
		{
			//printf("created pipe_ctp.\n");
		}


		// open history pipe from child-to-parent
		if(pipe(pipe_hist) == -1)
		{
			fprintf(stderr, "could not create pipe_hist!\n");
		}
		
		pid = fork();

		if (pid < 0)
		{
			fprintf(stderr, "could not fork!\n");
		}
		else
		{
			if (pid == 0) // child process
			{
				//free(user_input);

				char* buf = (char*) malloc(MAX_LINE * sizeof(char));

				//printf("%u: in child process.\n", getpid());

				// close unused write end of parent-to-child pipe
				close(pipe_ptc[1]);

				// close unused read end of child-to-parent pipe
				close(pipe_ctp[0]);
				
				// close unused read end of child-to-parent history pipe
				close(pipe_hist[0]);
				
				//printf("%u: reading from pipe.\n"}, getpid());

				read(pipe_ptc[0], buf, MAX_LINE * sizeof(char));

				//printf("%u: read message into buffer: %s\n", getpid(),	buf);

				int t = tokenize(buf, args);
				
				// write command to history pipe
				
				write(pipe_hist[1], buf, MAX_LINE * sizeof(char));
				//printf("buf: %s (%db)\n", buf, MAX_LINE * sizeof(char));			

				//printf("t = %d\n", t);

				free(buf);

				// t:
				//-1: first char is NULL, do nothing 
				// 0: Continue normally without waiting
				// 1: Exit
				// 2: & character found, wait for child to terminate
				// 3: history
				// 4: !! found, exec last command in history queue
				// 5: !n found, exec nth command in history queue

				if (t >= 0) // not -1, so execute a command
				{
					//char tch = t + '\0';
					// write state flag using child-to-parent pipe
					//write(pipe_ctp[1], &tch, 1);
					
					if (t == 1)
					{
						// write exit flag via child-to-parent pipe
						write(pipe_ctp[1], "1", 1);	
					}
					else
					{
						switch(t)
						{
							case 2:
								// write wait flag
								write(pipe_ctp[1], "2", 1);
								break;
							case 3:
								// write history flag
								write(pipe_ctp[1], "3", 1);
								break;
							case 4:
								// write last command flag
								write(pipe_ctp[1], "4", 1);
								break;
							case 5:
								// write nth command flag
								write(pipe_ctp[1], "5", 1);
								break;
							default:
								break;
						}

					}
					
					execvp(args[0], args);
				}

				should_run = 0;
			}
			else
			{
				//printf("%u: in parent process.\n", getpid());

				// close unused read end of parent-to-child pipe
				close(pipe_ptc[0]);

				// close unused write end of child-to-parent pipe
				close(pipe_ctp[1]);

				// close unused write end of history pipe
				close(pipe_hist[1]);

				if (user_input)
				{
					//printf("%u: writing to pipe.\n", getpid());
					
					write(pipe_ptc[1], user_input, MAX_LINE * sizeof(char));

					// the state flag will indicate various states the main
					//  process should take
					//-1: first char is NULL, do nothing 
					// 0: Continue normally without waiting
					// 1: Exit
					// 2: & character found, wait for child to terminate
					// 3: history
					// 4: !! found, exec last command in history queue
					// 5: !n found, exec nth command in history queue
					
					char state_flag = ' ';

					read(pipe_ctp[0], &state_flag, 1);

					int flag = atoi(&state_flag);
					
					//printf("state_flag = %d\n", flag);
					
					char* h = (char*) malloc(MAX_LINE * sizeof(char));
					read(pipe_hist[0], h, MAX_LINE * sizeof(char));
					
					//printf("h: %s\n", h);

					int n = 0;
					switch(flag)
					{
						case -1:
						case 0:
							// ignore SIGCHLD so kernel will automatically
							// reap zombies
							signal(SIGCHLD, SIG_IGN);
							break;
						case 1:
							should_run = 0;
							//printf("\nflag is 1. set should_run to 0.\n");
							break;
						case 2:
							//printf("\nwaiting for child to execute.\n");
							wait(NULL);
							break;
						case 3:
							GetHistory(history);
							break;
						case 4:
							h = realloc(h, strlen(lastInput(history)));
							h = lastInput(history);
							printf("%s\n", h);
							
							hist_command = (char*) malloc(strlen(h));
							strcpy(hist_command, h);
							
							run_com_from_hist = 1;
							break;
						case 5:
							//printf("h: %s (%ub)\n", h, strlen(h));
							n = atoi(h+1);
							
							if (n <= history->count)
							{							
								h = realloc(h, strlen(nInput(history, n)));
								h = nInput(history,n);
								
								hist_command = (char*) malloc(strlen(h));
								strcpy(hist_command, h);
								
								hist_command[strlen(hist_command)] = '\0';
								
								run_com_from_hist = 1;
							}
							break;
						default:
							break;
					}
					
					
					// do not add nulls or history command to command history
					switch(flag)
					{
						case -1:
						case 3:
							break;
						case 4:
						case 5:
							//enQueue(history, hist_command);
							break;
						default:
							enQueue(history, h);
							break;
					}
					
				}
			}
		}
		//free(user_input);
   	} 

	free (user_input);

	return 0;
}
